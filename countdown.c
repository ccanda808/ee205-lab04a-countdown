///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//
//   Years: 7 Days: 21: Hours: 22: Minutes: 42: Seconds: 46
//
// @author Cameron Canda <ccanda@hawaii.edu>
// @date  10_February_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define MINUTE 60
#define HOUR   3600
#define DAY    86400
#define YEAR   31536000

//Reference time:  Tue Jan 21 04:26:07 PM HST 2014

int main() {

   struct tm str_reftime;
   struct tm *str_nowtime;
   time_t time_ref, time_now, time_nowlocal;
   double diff;
   int n;

   str_reftime.tm_sec   =   7;
   str_reftime.tm_min   =  26;
   str_reftime.tm_hour  =  16;
   str_reftime.tm_mday  =  21;
   str_reftime.tm_mon   =   0;
   str_reftime.tm_year  = 114;
   str_reftime.tm_wday  =   2;
   str_reftime.tm_yday  =  21;
   str_reftime.tm_isdst =   0;

   time_ref = mktime(&str_reftime);

   printf("Reference time: Tue Jan 21 04:26:07 PM HST 2014\n\n");

   while(1){

      time(&time_now);
      str_nowtime = localtime(&time_now);
      time_nowlocal = mktime(str_nowtime);
      diff = time_nowlocal - time_ref;
      n = (int) diff;

      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",
            (n/YEAR), ((n%YEAR)/DAY), ((n%DAY)/HOUR), ((n%HOUR)/MINUTE), (n%MINUTE));

      sleep(1);

   }
   return 0;
}
